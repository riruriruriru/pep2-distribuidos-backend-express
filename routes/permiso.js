var express = require('express');
var permisos = express.Router();

var PermisosCtlr = require('../controllers/permiso.js');

permisos.route('/all').get(PermisosCtlr.findAllPermisos)
permisos.route('/add').post(PermisosCtlr.createPermisos)
permisos.route('/getId/:permisoID').get(PermisosCtlr.findPermisoById)
permisos.route('/getIdCassandra/:permisoID').get(PermisosCtlr.findByIdCassandra)
permisos.route('/allCassandra').get(PermisosCtlr.findAllCassandra)
/* GET users listing. */


module.exports = permisos;
