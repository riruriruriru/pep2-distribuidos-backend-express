const bodyParser = require('body-parser')
const express = require('express')
const router = express.Router()
router.use(bodyParser.json())
var moment = require('moment');
const { Permiso } = require('../model/sequelize')
const {models} = require('../models/Cassandra')

//app.post('/api/permiso/create', (req, res) => {
//    Permiso.create(req.body)
//        .then(permiso => res.json(permiso))
//})
// get all users
exports.createPermisos= (req, res)=>{
    var fechaLimite = moment().add(3, 'hours');
    var fechaInicioCassandra = moment().format("YYYY-M-D HH:MM");
    var fechaLimiteCassandra = moment().add(3,'hours').format("YYYY-M-D HH:MM");
    var permiso = {
        nombre: req.body.nombre,
        rut: req.body.rut,
        comuna: req.body.comuna,
        region: req.body.region,
        direccion: req.body.direccion,
        motivos: req.body.motivos,
        fechaPermisoLimite: fechaLimite
    }
    Permiso.create(permiso)
    .then(response => {
    var permisoObject = {
        id: response.id,
        nombre: req.body.nombre,
        rut: req.body.rut,
        comuna: req.body.comuna,
        region: req.body.region,
        direccion: req.body.direccion,
        motivos: req.body.motivos,
        fechaPermisoLimite: fechaLimiteCassandra,
        fechaCreacion: fechaInicioCassandra
    }
    var permisoCassandra = new models.instance.PermisoCas(permisoObject);
    permisoCassandra.save(function(err){
        if(err) console.log(err);
        else console.log('Yuppiie!');
        });

    res.status(200).send(response)
})

}

exports.findAllCassandra = (req, res) =>{
    models.instance.PermisoCas.find({}, function(err, permisos){
    if(err) {
        console.log(err);
        return;
    }
    //Note that returned variable john here is an instance of your model,
    //so you can also do john.delete(), john.save() type operations on the instance.
    res.send(permisos);
    });
}

exports.findByIdCassandra = (req, res) =>{
    models.instance.PermisoCas.findOne({id: parseInt(req.params.permisoID)}, function(err, permiso){
    if(err) {

        return res.status(500).send({message: "No existe permiso con el ID consultado"})
        
    }
    if(permiso===null||permiso===undefined){
        return res.status(500).send({message: "No existe permiso con el ID consultado"})
    }
    //Note that returned variable john here is an instance of your model,
    //so you can also do john.delete(), john.save() type operations on the instance.
    var fechayhoraactual = moment();

    if(fechayhoraactual.isBefore(permiso.fechaPermisoLimite)===true&&fechayhoraactual.isAfter(permiso.fechaCreacion)){
            return res.status(200).send({permiso: permiso, message: "Permiso es válido", bool: true}); // true
        }
    else{
            return res.status(200).send({permiso: permiso, message: "Permiso no es válido", bool: false});   
        }
    });
}

exports.findAllPermisos = (req, res) => {
    Permiso.findAll()
    .then(permisos => {
        res.send(permisos);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving permisos."
        });
    });
};
exports.findPermisoById = async (req, res) =>{
    //const permiso = await Permiso.findByPk(req.params.permisoID).catch(res.status(500));
    
    try{
        const permiso = await Permiso.findByPk(req.params.permisoID)
        if (permiso === null) {
          res.status(500).send({message: "No existe permiso con el ID consultado"})

          return
        } 
        else {
            var fechayhoraactual = moment();
            if(fechayhoraactual.isBefore(permiso.fechaPermisoLimite)===true&&fechayhoraactual.isAfter(permiso.fechaCreacion)){
                return res.status(200).send({permiso: permiso, message: "Permiso es válido", bool: true}); // true
            
            }
            else{
                return res.status(200).send({permiso: permiso, message: "Permiso no es válido", bool: false});   
            }
        
      // Its primary key is 123
    }
    }
    catch(error){
        return res.status(500).send({message: error.message || "Postgresql no disponible..."})
    }


}
