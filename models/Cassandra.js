var ExpressCassandra = require('express-cassandra');
const cassandra = require('cassandra-driver');
var models = ExpressCassandra.createClient({
    clientOptions: {
        contactPoints: [process.env.cassandraIP1, process.env.cassandraIP2],
        protocolOptions: { port: 9042 },
        keyspace: 'mykeyspace',
        queryOptions: {consistency: ExpressCassandra.consistencies.one},
        authProvider: new cassandra.auth.DsePlainTextAuthProvider('cassandra','Lz1AS9ZoBmry')
    },
    ormOptions: {
        defaultReplicationStrategy : {
            class: 'SimpleStrategy',
            replication_factor: 2
        },
        migration: 'safe',
    }
});

var PermisoCassandra = models.loadSchema('PermisoCas', {
    fields:{
          id:"int",
          nombre: "varchar",
          rut: "varchar",
          comuna: "varchar",
          region: "varchar",
          direccion: "varchar",
          motivos: "varchar",
          fechaCreacion: "timestamp",
          fechaPermisoLimite: "timestamp"
    },
    key:["id"]
});

// MyModel or models.instance.Person can now be used as the model instance
console.log(models.instance.PermisoCa === PermisoCassandra);

// sync the schema definition with the cassandra database table
// if the schema has not changed, the callback will fire immediately
// otherwise express-cassandra will try to migrate the schema and fire the callback afterwards
PermisoCassandra.syncDB(function(err, result) {
    if (err) throw err;
    // result == true if any database schema was updated
    // result == false if no schema change was detected in your models
});
module.exports = {
    models
}
