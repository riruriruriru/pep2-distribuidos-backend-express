const { Sequelize, DataTypes, Model } = require('sequelize');


module.exports =(sequelize, type)=>{
  return sequelize.define('Permiso',{
  id: {
  	type: DataTypes.INTEGER,
  	primaryKey: true,
  	autoIncrement: true
  	},
  nombre: {
    type: DataTypes.STRING,
    allowNull: false
  },
  rut: {
    type: DataTypes.STRING,
    allowNull: false
  },
  comuna: {
    type: DataTypes.STRING
    // allowNull defaults to true
  },
  region: {
    type: DataTypes.STRING
    // allowNull defaults to true
  },
  direccion: {
    type: DataTypes.STRING
    // allowNull defaults to true
  },
  motivos:{
    type: DataTypes.STRING
  },
  fechaCreacion: {
    type: DataTypes.DATE,
    defaultValue: Sequelize.NOW
    // This way, the current date/time will be used to populate this column (at the moment of insertion)
  },
  fechaPermisoLimite: {
    type: DataTypes.DATE,
    // This way, the current date/time will be used to populate this column (at the moment of insertion)
  }
  })
 }

