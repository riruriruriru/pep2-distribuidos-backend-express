# Wiki Pep 2 Sistemas Distribuidos:

# Wiki Pep 1 Sistemas Distribuidos:

## Definición Sistema Distribuido:
- "Un sistema distribuido es un sistema en el cual los componentes de hardware o software localizados en una red de computadores pueden comunicarse y coordinar sus acciones mediante el paso de mensajes"
- "Una colección de computadores independientes que para el usuario parecen ser un sólo sistema coherente"
## Análisis de un Sistema Distribuido:
Un sistema distribuido debe cumplir ciertas características básicas o características deseables.
### Características de un Sistema Distribuido:
#### Transparencia: 
La transparencia debe esconder el hecho de que procesos y recursos están distribuidos en múltiples computadores, por lo tanto para el usuario o aplicaciones debe parecer un sistema único. La transparencia se relaciona con el acceso, ubicación, migración, re-localización, replicación, concurrencia y fallas. Además ayuda a lidiar con la heterogeneidad del hardware.
#### Escalabilidad:
Puede lograrse de forma horizontal (aumentar cantidad de recursos) o vertical (aumentar capacidad de recurso). Además esto se puede lograr mediante el particionar (dividir y distribuir), replicar (copiar en otro lugar) y cache (recursos de acceso fácil y rápido)
#### Disponibilidad:
El sistema construido debe ser confiable, debido a que tanto hardware como software pueden fallar. Una forma de lograr disponibilidad es mediante la réplica de recursos.
#### Apertura:
El sistema distribuido abierto debe ofrecer servicios bajo reglas estándar de sintaxis o semántica. Debe permitir interoperabilidad, portabilidad y facil extensión.
## Descripción Sistema Legado
El sistema legado para obtener permisos fue desarrollado con una arquitectura simple de Cliente-Servidor-Base de datos.
### Servidor:
Para el servidor, se utilizó el framework de backend Express, basado en el lenguaje NodeJS. El objetivo principal de Express es ser un "framework de desarrollo de aplicaciones web minimalista y flexible para Node.js". Las características principales de Node.js y Express son las siguientes:
##### Node.js
- Gran rendimiento y escalabilidad en aplicaciones web en tiempo real.
- Lenguaje JavaScript, el cual es simple de utilizar y ofrece mejores características para el desarrollo web que PHP o Python.
- Gestor de paquetes NPM que facilita y proporciona el acceso a miles de paquetes.
- Portable: Versiones disponibles para Windows, OS X, Linux, Solaris, WebOS, etc.
##### Express
- Manejo de peticiones con diferentes verbos HTTP y diferentes URL o rutas.
- Permite establecer el puerto para conectar, procesamiento de peticiones "middleware", librerías para trabajar con cookies, sesiones, parámetros URL, datos POST, etc.
#### Descripción Servidor:
El servidor se utiliza para implementar una API REST, que mediante HTTP realiza operaciones PUT y GET para ingresar y obtener permisos desde una base de datos. Además, se utiliza la librería "sequelize" para abstraer el uso de la base de datos e implementar una ORM (object relational database), pudiendo crear un simple modelo (que se traduce a una tabla en la base de datos) y un controlador que permite realizar las operaciones GET o POST.
### Cliente:
La parte del cliente o frontend se desarrolló utilizando el framework ReactJS, el cual también está basado en JavaScript, entre sus principales características se encuentran:
- Desarrollo de aplicaciones web ordenado y menos verboso.
- Permite asociar vistas con los datos, para cuando se cambie o modifique un dato, también se actualice la vista.
- Manejo de componentes que permiten encapsular funcionamiento y presentación.
- Gracias al uso de Virtual DOM, cuando se actualiza una vista, React actualiza el DOM virtual en vez del real, lo cual es más rápido. 
- Por lo tanto, cuando React compara el Dom virtual con el real, sabe qué partes específicas debe actualizar y se ahorra el actualizar la vista completa. Esto se hace de forma transparente para el desarrollador.
- Gracias a estar basado en componentes, estos se pueden reutilizar en el mismo proyecto o en otros proyectos.
#### Descripción Cliente:
Para utilizar las ventajas de react, se crearon varios componentes distintos para representar el header, footer y el formulario en sí. Mediante la librería Axios se realizan las request al servidor. También se utiliza la api "División Político Administrativa" para obtener y mostrar las regiones de Chile y sus comunas.
### Base de datos:
La base de datos utilizada es postgresql, que corresponde a un open-source RDBMS, lo que significa que es un Sistema Gestor de Bases de Datos Relacionales cuyos focos principales son la Extensibilidad(facultad de flexibilidad para el cambio como soportar nuevas funcionalidades) y el Cumplimiento de Estándares. Las caracteristcias principales de postrgresql son las siguientes:
##### Postgresql
- Control de Concurrencia Multi-Versión, lo que permite mantener "instantaneas" o bien versiones de los datos, lo que impide que las instrucciones vean inconsistencia en los datos producidas por transacciones concurrentes que realizan actualizaciones en las mismas filas de datos
- La Recuperación point-in-time ofrece a PostgreSQL la habilidad de recuperar datos de una forma simple, rápida y de forma íntegra, restaurando el estado de la base de datos a un punto anterior en el tiempo
- Facilidad y agilidez en la manipulacion de objetos, gracias a su integracion con paquetes compatibles con Node.js con la ayuda de Sequelize ORM, compatible ademas con otros gestores como MySQL, SQLite y MSSQL.
## Analisis Sistema Legado
El siguiente analisis considera las carectersiscticas implementadas en el sistema legado, y las caractersiticas deseables que debe considerar un sistema distribuido
### Transparencia
- **Acceso**: Gracias a la utilizacion del gestor de paquetes de Node.js (npm), tanto local como remotamente, basta con utilizar los comandos respectivos para instalar cada uno de los modulos que requiere el sistema para funcionar y también para ponerlo en ejecución, mas aún, gracias a la capa de abstraccion proporcionada por los contenedores de docker, independiente del sistema operativo en el que se encuentre, se tiene el mismo acceso con los mismos comandos para "buildear" el contenedor y levantarlo.

- **Ubicación**: El sistema no con cuenta con un dominio proporcionado por el protocolo DNS, por lo que practicamente el cliente trabaja directamente con la dirección IP del servidor en el que se esta ejecutando la aplicación.

- **Migración**: De la mano con lo que es el acceso, y gracias al complemento tanto del gestor de paquetes de Node.js "npm", y a los contenedores de docker, la migracion se hace bastante sencilla ya que basta con realizar un par de comandos sobre el contenedor de docker que contiene el sistema, para buildear y levantar este independiente del sistema operativo del servidor.

- **Re-localización**: De mano con la Ubicación, al no tener un sistema de nombres, re-localizar el sistema a otro servidor supone trabajar con una nueva direccion IP y por tanto dar a conocer al usuario que el sistema esta siendo movido a otro lugar y posiblemente dejar inhabilitada la direccion anterior para ser utilizada normalmente.

- **Replicación**: El sistema no cuenta con replicación pues consta solamente de una base de datos y un servidor que corre el backend y frontend. Sin embargo, si existiera, y gracias a la arquitectura Cliente-Servidor-BD y al implementar el sistema como una API-REST, la replicacion se asegura transparente. Gracias a docker, esto se podría realizar independientemente del Sistema Operativo.

- **Concurrencia**: A traves del objeto "Request" proporcionado por el framework de express, la concurrencia de solicitudes se da de forma transparente, sin evidenciar a los usuarios que estan accediendo a un mismo recurso (backend y base de datos).

- **Falla**: En cuanto a transparencia de fallas, no lo es un su totalidad, pero dependiedo del tipo de falla esta podria denotarse de manera transparente o no, por ejemplo alguna falla a nivel de back-end no afecta en ninguna medida al correcto despliegue del front-end, de igual manera si fallara la base de datos.

### Escalabilidad
- **Vertical**: Gracias a la implementacion de contenedores de software para montar la API con docker, la escalabilidad vertical se presenta de manera mucho mas sencilla, ya que esta capa de abstraccion dota al sistema de una versatilidad a la hora de migrar o cambiar el servidor por uno de mas capacidad, independiente de la plataforma sobre la cual se desempeñe este nuevo servidor. De igual manera, tambien se encuentra disponible la escalabilidad vertical considerando solamente modificaciones de los recursos ya disponibles en el servidor actual que potencie sus capacidades.

- **Horizontal**: Si bien no hay escalabilidad horizontal, el hecho de utilizar contenedores facilita el posible escalamiento vertical del sistema simplemente agregando nuevos nodos o servidores, para equilibrar mejor la carga del sistema, independiente del sistema operativo en cual se desenvuelvan estos nuevos nodos. En este sentido, los nuevos nodos solamente necesitaran tener instalado docker y a traves de los comandos "build" y "up" se montara el sistema en un contenedor disponible para su despliegue. De forma paralela, se puede trabajar la escalabildad por el lado de la base de datos, replicando la ya existente hacia otras tales como Apache Cassandra o Apache HBase, lo que se considera para futuras modificaciones, facilitado ademas por los paquetes ofrecidos por Node.js con los ORM de Cassandra y HBase, u alguna otra base de datos distribuida. Por lo tanto, aunque no hay escalabilidad horizontal, solo agregando el uso de algunas tecnologías (como Kubernetes, otras bases de datos), podría lograrse utilizando los mismos contenedores de backend y front end.

### Disponibilidad
Actualmente el sistema no cuenta con una disponibilidad altamente tolerante a fallos, a pesar de que testeandolo alcanza a soportar un promedio de 3000 usuarios en un minuto o quizas un poco mas sin incurrir en fallas, ante una eventual no cuenta con mecanismos para manejarlos, tales como replicas de nodos o la base de datos, por lo tanto tampoco est disponible la opcion de balancear la carga en caso de que algun servidor o la misma base de datos falle, aun asi tomando en cuenta la horizontalidad que si esta disponible en este sistema, facilmente se pueden agregar nuevos servidores y bases de datos como ya se menciona, para mejorar la capacidad de maniobra del sistema ante posible fallas. Considerando ademas que la base de datos esta implementada en base a PostgreSQL, este ultimo prtmite manejar la base de datos arquitecturalmente como una relacion Maestro-Esclavo, por lo que se puede destinar una base de datos maestra, apoyada por uno o mas servidores de respaldo listos para tomar accion en caso de que el servidor primario falle, ahora bien, esta implementacion se proyecta para futuras modificaciones.
### Apertura
La apertura del sistema esta presente desde el momento en que este esta implementado en forma de una API REST, que le permite se utilizada por otro software como capa de abstracción. Es Portable, gracias al uso de contenedores de docker facilita el despliegue del sistema en multiples sistemas operativos. Es de facil extension, considerando que componentes nuevos pueden ser agregados gracias al sistema gestor de paquetes de Node.js, ubicados en el repositorio npm. Los servicios proporiconados por el sistema se rigen por reglas estandar utilizando el protocolo HTTP para llevar a cabo operaciones GET y PUT sobre la base de datos.

## Test
Para analizar la capacidad de respuesta, se utilizó Artillery.io, con el cual se simularon request POST para el backend de la aplicación.
Se generó un archivo .yml con un test el cual tiene las siguientes características:
- **Duración**: 60 segundos
- **Tasa de llegada**: 50 usuarios por segundo.
- **Usuarios totales**: El test se divide en 6 etapas de 10 segundos, por lo tanto se tiene un total de 3000 usuarios virtuales en un minuto.
- **Request**: Todos los usuarios hacen un request POST en el cual generan un permiso nuevo.

### Resultados
Se obtuvieron los siguientes resultados:



![alt text](testBackPostPep1.png "Test Back: Solicitudes POST")

De las 3000 solicitudes, se pueden desprender los siguientes datos:
- Las 3000 solicitudes fueron exitosas, debido a que se recibieron 3000 respuestas "200"
- Se completaron los 3000 escenarios y las 3000 solicitudes satisfactoriamente.
- Se tuvo un tiempo de respuesta mínimo de 290 microsegundos y un promedio de 306 microsegundos, el máximo fue de 341 microsegundos, por lo tanto el tiempo de respuesta de la aplicación fue consistente.


# Descripción Sistema Pep 2:

Para esta segunda parte, en base a los resultados obtenidos en la primera parte y a su respectivo análisis, se procede a realizar un análisis más detallado de aquellas caracteristicas que se espera que sean cubiertas por una buena implementacion de un sistema distribuido, que por una u otra razon no se lograron concretar anteriormente, y en base a este análisis, se realizan modificaciones en la arquitectura del sistema para dar cobertura a estas y se describen las características particulares en que ayudan y como lo logran.

Entonces aquellas caracteristicas deseables de un sistema distribuido que no son solventadas por el sistema legado son las siguientes:

- Transparencia (Ubicación)
- Transparencia (Re-localización)
- Transparencia (Replicación)
- Transparencia (Falla)
- Escalabilidad (Replicación)
- Disponibilidad

Existen diversas formas de cubrir las caracteristicas anteriores, ahora bien, en cuanto a modificaciones a nivel arquitectural se sugieren (de forma general) las que se detallan a continuacion:

- Transparencia (Ubicación y Re-localización): Dotar al sistema de un dominio que permita identificarlo y encontrarlo facilmente via online, (como es el caso de
www.comisariavirtual.cl), lo que se traduce en la incorporación de un servidor DNS a la arquitectura ya existente, que mantenga en su base de datos el nombre del dominio, y la respectiva direccion IP del servidor de hosting en el cual esta siendo desplegado el sistema. De esta forma el cliente ya no estará trabajando directamente con la direccion IP del servidor en que se encuentra corriendo el sistema, sino que a traves de un nombre mapeado a esa dirección, y precisamente es a este nombre por el que el cliente preguntara en todo momento, independiente de que la direccion IP del servidor hosting sea modificada ya sea por migraciones u otro motivo. En caso de que cambie el hosting y por lo tanto la dirección IP, esto será transparente para el usuario ya que seguirá accediendo a la página por el nombre de esta y no su dirección.

- **Transparencia (Replicación)**: Para cumplir esta caracteristica requiere de la anterior para estar cubierta, pues es con esta que el usuario no nota que algun componente del sistema esta replicado y que internamente no siempre se consulta una misma base de datos o backend. Nuevamente esto se puede solucionar con un DNS o con un balanceador de carga.

- **Escalabilidad (Replicación)**: Una buena idea consiste en aprovechar los servicios proporcionados por Google Cloud, en especifico a los contenedores de Kubernetes Engine, que de forma similar a lo que realiza docker, permite empaquetar aplicaciones, desplegandolas y ejecutandolas de forma facil en su propio entorno aislado. Particularmente a lo que escalabilidad se refiere, kubernetes ofrece una "autoescalabilidad" tanto a nivel horizontal como vertical, entonces arquitecturalmente el sistema sería desplegado y ejecutado dentro de un cluster de la Plataforma de Google Cloud, repartido en un cierto numero de nodos, los cuales pueden escalar en memoria lo que vendria a cubrir la escalabilidad vertical. Por otro lado, tanto el back-end como el front-end estaran replicados en un determinado numero de contenedores o pods, los cuales se autobalancean de acuerdo a la carga ejercida sobre el sistema y deployandose automaticamente entre los nodos. A nivel de base de datos, se puede replicar la informacion ya contenida en la base de datos de postgresql, a otra como cassandra por ejemplo, todo manejado gracias al servicio de Google Cloud Datastore el cual escala automaticamente y sin problemas en función de los datos.

- **Disponibilidad**: Como bien se menciona en la primera parte, una de las formas de lograr disponibilidad consiste en replicar recursos para justamente dotar al sistema de cierta autonomía de maniobra ante posibles fallas, lo cual se logra a traves del autobalance de los pods ya sea del back-end o el front-end, que aseguran que el sistema se encuentre disponible en caso de que una instancia no sea suficiente, autoescalando y desplegando un nuevo pod en cualquiera de los nodos del cluster en el que el sistema se despliega, y que son replicas exactas unas de otras. De igual forma la disponibilidad de la base de datos se asegura teniendo un GCD (Google Cloud Datastore) para una base de datos primaria a la cual se realizan las consultas en primera instancia como lo es postgresql en este caso, y un GCD de cassandra la cual es consultada en caso de que la primaria no se encuentre disponible, recordar que esta ultima es una replica en cuanto a datos de la base de datos primaria. 

- **Transparencia (Falla)**: La empaquetación de la aplicación provista por los servicios de google cloud platform, en lo que respecta a kubernetes, tal como se menciona en algun punto beneficia ademas a lo que respecta a la perspectiva del usuario relativo a posibles fallas en el sistema, ya que gracias al factor de replicacion de la base de datos, que además de depender de la base de datos primaria en postgresql ahora tambien contaría con la asistencia de una base de datos auxiliar, esencialmente para estos casos en los que por alguna razon la base de datos de postgresql no se encuentre disponible. Entrando aun mas en detalle de lo que se podria lograr en cuanto a materia de transparencia a fallos se refiere, se encuentran.

  - En caso de que la demanda por parte de los usuarios sea demasiada, se podria deployar de forma automatica pods adicionales tanto para el back-end y el front-end para suplir dicha demanda y evitando que el sistema incurra en algun tipo de falla por exceso de consultas.
  - Mientras haya al menos un contenedor de front-end y uno de back-end siendo desplegados, independiente del numero de replicas que incurran en fallos, estas seran transparentes para el usuario
  - De igual manera con las bases de datos, tieniendo un cluster con cassandra y replicación, suponiendo que falla algun nodo del cluster, también sería transparente para el usuario

### Desarrollo PEP 2:
Para la PEP 2, se implementan los cambios mencionados en los puntos descritos anteriormente en negrita, los cuales son:
- Transparencia (Replicación)
- Escalabilidad (Replicación)
- Disponibilidad
y parcialmente:
- Transparencia (Fallas) 
## Cambios en Arquitectura:
Se utilizan contenedores Docker para el frontend y backend, los cuales se les realiza un "build" y se suben al registro de google cloud platform, de esta forma, dichos contenedores pueden ser utilizados por Google Kubernetes Engine. 
La parte central de la nueva arquitectura consiste en un cluster de GKE, el cual cuenta con 3 nodos. Por lo tanto, en este GKE se deployean en forma de "Workloads" los contenedores Docker del frontend y backend, ambos con la opción de autobalanceo entre 1 a 5 pods, dependiendo del porcentaje de uso del CPU, es decir, cuando un pod use más del 50% de su CPU, será deployeada una nueva réplica de forma completamente transparente.
Además, se inicializan dos "Servicios" que se encargan de exponer una IP común para todos los pods del backend y todos los pods del frontend, lo cual funciona como un balanceador de carga. Por lo tanto, si el backend actualmente tiene 5 réplicas, gracias al balanceador de carga solo existirá 1 IP expuesta y dicho servicio se encargará de redirigir al pod mejor preparado para recibir la solicitud actual.
Respecto a las bases de datos, se deployea una base de datos postgres en un Google Compute Engine, el cual no posee autoescalado, debido a que si se desea re-deployear en un GCE con más capacidad, se debe reiniciar completamente la instancia de GCE. En cuanto a cassandra, se deployea un cluster de dos nodos de GCE con un datacenter de cassandra.
Por lo tanto, como ya se mencionó, se cumplen las siguientes características de sistemas distribuidos que no se cumplían anteriormente:
- Transparencia (Replicación): Deployeo transparente y automático de réplicas de backend y frontend, además de réplicas de base de datos transparentes para el usuario.
- Escalabilidad (Replicación): Servicio de autoescalado y balanceador de carga en GKE, que aporta escalabilidad horizontal y vertical. Además de una escalabilidad horizontal gracias al cluster de cassandra.
- Disponibilidad: Gracias al balanceador de carga y autobalanceo, se puede lograr una gran disponibilidad del sistema que está directamente relacionada con la demanda actual del sistema.

- Transparencia (Fallas): En casos específicos las fallas serían transparentes para el usuario, por ejemplo si fallan algunos pods de backend o frontend pero siguen habiendo disponibles al menos uno de cada uno, el usuario no se daría cuenta de estas fallas. También en el caso en que sólo cassandra (completamente o uno de sus nodos) falle, no afectaría a la base de datos postgres y el backend seguiría funcionando correctamente. 
### Diagrama de Componentes con cambios realizados:
![alt text](Diagramas_PEP_2_Distribuidos_-_Diagrama_Componentes.jpeg "Diagrama de Componentes")
### Diagrama de Despliegue con cambios realizados:
![alt text](Diagramas_PEP_2_Distribuidos_-_Diagrama_Despliegue.jpeg "Diagrama de Despliegue")

## Cambios en software/código:
Para implementar los cambios en la arquitectura (Uso de Kubernetes para front y back, además de replicación con una base de datos Cassandra de dos nodos) se realizaron las siguientes modificaciones:
- Modificación de ambos Dockerfile para permitir el correcto build para el posterior deploy en google kubernetes engine.
- Instalación con npm de express-cassandra y cassandra-driver para permitir la conexión entre express y cassandra, además de facilitar el manejo de las query gracias a las capacidades de ORM que ofrece express-cassandra.
- Creación de un modelo para el permiso que es utilizado por express-cassandra.
- Creación de funciones en controlador para consultar y guardar objetos en cassandra.
- Se cambia la función controlador que crea permisos, para que también cree permisos en cassandra.
- Se cambian los datos de conexión backend-base de datos y frontend-backend a variables de entorno ingresadas con los yaml de deploy de kubernetes.

## Bonus:

Según lo mencionado en el enunciado, el bonus implementado permite consultar la validez de un permiso ingresando su ID. Internamente, a nivel de frontend esto se implementó como un nuevo componente, que contiene el formulario para ingresar el ID a consultar y que renderiza el resultado de la consulta. Además, se tiene un manejo de errores en caso de que la consulta a la base de datos psotgres falle, se consultará la base de datos cassandra.
A nivel de backend, se implementaron dos funciones que reciben el ID consultado, buscan el permiso ya sea en la base de datos postgres o en cassandra y en base a la hora actual comprueban si el permiso consultado (en caso de existir) se encuentra dentro de la ventana de validez (3 horas desde su creación), en caso de existir y ser válido, se retornan los datos del permiso y un identificador que indica su validez.
Luego, estos datos se reciben en el frontend y se renderiza la vista con la respuesta de la solicitud al backend.
Por lo tanto, este bonus utiliza la nueva arquitectura de la aplicación y es tolerante (en cierto grado) a fallas, debido a que:
- Si falla el frontend, solo fallaría un "pod", por lo tanto una falla del frontend sería percibida por el usuario sólo si fallan todos los pods o el cluster completo.
- Respecto al backend, se tiene el mismo caso que en el frontend, la falla sería visible para el usuario sólo si el cluster completo falla o si fallan los 5 pods posibles del backend.
- Respecto a la base de datos, si falla postgres, se tiene de respaldo cassandra, la cual a su vez también tiene tolerancia a fallos gracias a que se tienen 2 nodos con cassandra y un factor de replicación igual a 2, es decir, todos los datos se encuentran en ambos nodos.


## Test Pep 2
### Test 1
Para analizar la capacidad de respuesta, se utilizó Artillery.io, con el cual se simularon request POST para el backend de la aplicación.
Se generó un archivo .yml con un test el cual tiene las siguientes características:
- **Duración**: 60 segundos
- **Tasa de llegada**: 50 usuarios por segundo.
- **Usuarios totales**: El test se divide en 6 etapas de 10 segundos, por lo tanto se tiene un total de 3000 usuarios virtuales en un minuto.
- **Request**: Todos los usuarios hacen un request POST en el cual generan un permiso nuevo.

### Resultados Test 1
Se obtuvieron los siguientes resultados:



![alt text](testBackPost.png "Test Back: Solicitudes POST")

De las 3000 solicitudes, se pueden desprender los siguientes datos:
- Las 3000 solicitudes fueron exitosas, debido a que se recibieron 3000 respuestas "200"
- Se completaron los 3000 escenarios y las 3000 solicitudes satisfactoriamente.
- Se tuvo un tiempo de respuesta mínimo de 294 microsegundos y una mediana de 315 microsegundos, el máximo fue de 1335 microsegundos, siendo bastante peor que el tiempo máximo para el sistema legado, pero los valores de p95 y p99 se mantuvieron entre 331 y 355 microsegundos, indicando que un 99% de las solicitudes se completaron antes de 355 microsegundos y por lo tanto la performance general del sistema sigue teniendo un buen tiempo de respuesta.

### Test 2
Debido a que este nuevo sistema incluye la funcionalidad de consultar un permiso, se realizó un test GET, en el cual se generan usuarios virtuales que consulten la validez de un permiso.
Se generó un archivo .yml con un test el cual tiene las siguientes características:
- **Duración**: 120 segundos
- **Tasa de llegada**: 100 usuarios por segundo.
- **Usuarios totales**: El test se divide en 12 etapas de 10 segundos, por lo tanto se tiene un total de 12000 usuarios virtuales en dos minutos.
- **Request**: Todos los usuarios hacen un request GET en el cual consultan un permiso.


### Resultados Test 2

Se obtuvieron los siguientes resultados:



![alt text](testBackGetArtillery.png "Test Back: Solicitudes GET")

De las 12000 solicitudes, se pueden desprender los siguientes datos:
- 10001 solicitudes fueron exitosas, debido a que se recibieron 10001 respuestas "200"
- De las 12000 solicitudes, se completaron sólo 10001 escenarios.
- De los escenarios fallidos, se tuvieron  237 rechazos de conexión y 1762 "timed out", es decir, que con una pequeña modificación de software (a nivel del driver que conecta el backend con las bases de datos) se podría aumentar el tiempo de espera máximo para responder una solicitud, reduciendo quizás en su totalidad las 1762 fallas por tiempo de espera, o también se podría mejorar el CE que contiene la base de datos postgres o agregar nodos al cluster de cassandra.
- Se tuvo un tiempo de respuesta mínimo de 290 microsegundos y una mediana de 320 microsegundos, el máximo fue de 66042 microsegundos, debido a que gracias al flujo de las solicitudes, algunas no se resolvían inmediatamente. El p95 y p99 fueron similares al tiempo de respuesta máximo, indicando que efectivamente muchas solicitudes demoraron mucho tiempo en resolverse. A pesar de los "malos" resultados en cuanto a tiempo de respuesta, la cantidad de solicitudes completadas con código 200 se puede considerar satisfactoria.

Debido a que el GKE tiene un comportamiento de autoescalado si la utilización de CPU de un POD sobrepasa el 50%, se puede observar en la siguiente imagen que debido a la gran carga de usuarios, se deployearon en total 4 réplicas del backend, pudiendo asumir que si no se tuviera autoescalado, probablemente la performance del sistema hubiera sido bastante peor.
![alt text](testBackGet.png "Comportamiento Kubernetes Engine")

